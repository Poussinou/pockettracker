# PocketTRacker ![](app/src/main/res/mipmap-xxhdpi/ic_launcher.webp)
=============================

**A little app to keep track of your straight pool games.**

A pool player myself, I was always annoyed by writing the score sheet for straight pool games.
So here is a digital tool to get rid of this paperwork! 
It automatically keeps tabs on the scores. All you have to do is enter the number of balls after every turn and why the turn ended.

*This is just the first basic version, I have a lot of [additional features I plan on implementing](Ideas.md). Help is always welcome of course.*







