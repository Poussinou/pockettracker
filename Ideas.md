# Current Version
- allow changing of player names
- with player clubs
- keeps track of the scores
- revert button
- player focus with indicator
- scores are changed by difference of balls on table
- numberpane for entering of remaining balls
- entering of reason for end of turn
- scoresheet viewer

# Future features
- more layouts
- save games
- extended scoresheet with coloring
- remember players via profiles
- statistics for players and profiles with plots
- options menu for customization
- option of tracking by every pot
- settings for toggling options like club on/off
- snookertracker in same app
- maybe get number of remaining balls by camera (for users who are too lazy to count i guess)