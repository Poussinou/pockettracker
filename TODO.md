- allow click on playercard for details, statistics
- in that view allow for changing of player to see the others stats
- bug that kills game when scoresheet is opened from landscape
- save game capability
- run view for players
- rack counter in table?
- potentially tie all backend components into a Game Class
- encapsulated winner functionality
- club toggleable
- settings menu
- mark statistics and winner in table 
- table view landscape
- scoresheet switch button in header
- retry layouting switch button smaller
- add more final keywords to mark immutability
- find good statistics library